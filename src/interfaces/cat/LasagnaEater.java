package interfaces.cat;

public interface LasagnaEater {
    String getLasagnaRecipe();
}
