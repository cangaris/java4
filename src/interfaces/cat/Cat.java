package interfaces.cat;

public interface Cat {
    int NUMBER_OF_PAWS = 4;
    String getName();
}
