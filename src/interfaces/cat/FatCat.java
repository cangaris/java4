package interfaces.cat;

public interface FatCat extends Cat, LasagnaEater {
    double getWeight();
}
