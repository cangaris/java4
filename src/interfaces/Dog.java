package interfaces;

class Dog implements AnimalEat, AnimalRun {

    @Override
    public void eat() {
        System.out.println("jem karmę dla psa");
    }

    @Override
    public void run() {
        System.out.println("biegam sobie, wow wow");
    }

    @Override
    public void jump() {
        System.out.println("pies skacze sobie");
    }
}
