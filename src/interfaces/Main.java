package interfaces;

public class Main {
    public static void main(String[] args) {

        var cat = new Cat();
        var dog = new Dog();
        var bird = new Bird();
        var fish = new Fish();

        cat.eat();
        dog.eat();
        bird.eat();

        cat.run();
        dog.run();
        bird.run();

        bird.fly();

        fish.swim();
        fish.run();
        fish.eat();

        dog.jump();
        fish.jump();
    }
}
