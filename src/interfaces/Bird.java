package interfaces;

class Bird implements AnimalEat, AnimalRun, AnimalFly {

    @Override
    public void eat() {
        System.out.println("jem nasiona słonecznika rozsypane na ulicy ;)");
    }

    @Override
    public void run() {
        System.out.println("latam sobie");
    }

    @Override
    public void fly() {
        run();
    }
}
