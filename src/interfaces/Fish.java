package interfaces;

class Fish implements AnimalRun, AnimalEat, AnimalSwim {

    @Override
    public void eat() {
        System.out.println("zjadam planktom");
    }

    @Override
    public void run() {
        System.out.println("pływam sobie");
    }

    @Override
    public void swim() {
        run();
    }
}
