package interfaces;

public interface AnimalRun {

    void run();

    default void jump() {
        logJump("zwierze skacze sobie");
    }

    default void bigJump() {
        logJump("zwierze skacze wysoko");
    }

    private void logJump(String message) {
        System.out.println(message);
    }
}
