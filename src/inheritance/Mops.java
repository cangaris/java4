package inheritance;

public class Mops extends Pies {

    Mops() {
        super("mops");
    }

    @Override
    public String toString() {
        return "Mops{} " + super.toString();
    }
}
