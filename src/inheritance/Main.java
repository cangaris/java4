package inheritance;

public class Main {

    public static void main(String[] args) {

        var mops = new Mops();
        var owczarekNiemiecki = new OwczarekNiemiecki();

        owczarekNiemiecki.nazwijPsa("Szarik");
        mops.nazwijPsa("Shrek");

        owczarekNiemiecki.szekaj();
        owczarekNiemiecki.biegaj(5);

        System.out.println(owczarekNiemiecki);
        System.out.println(mops);
    }
}
