package inheritance;

public class BlednaNazwaPsaException extends RuntimeException {

    BlednaNazwaPsaException(String message) {
        super(message);
    }
}
