package inheritance;

public class OwczarekNiemiecki extends Pies {

    OwczarekNiemiecki() {
        super("owczarek niemiecki");
    }

    protected void biegaj() {
        System.out.println("klasa Owczarek : biegam sobie");
    }

    protected void biegaj(int i) {
        System.out.println("klasa Owczarek : biegam sobie " + i + " km");
    }

    @Override
    protected void szekaj() {
        System.out.println("klasa Owczarek : szczek szczek");
    }

    @Override
    public String toString() {
        return "OwczarekNiemiecki{} " + super.toString();
    }
}
