package inheritance;

public abstract class Pies {

    private String imie;
    private String rasa;

    Pies(String rasa) {
        sprawdzRase(rasa);
        this.rasa = rasa;
    }

    protected void szekaj() {
        System.out.println("klasa Pies : wow wow");
    }

    protected void nazwijPsa(String imie) {
        if (imie.charAt(0) == 'S') {
            this.imie = imie;
        } else {
            throw new BlednaNazwaPsaException("Nazwa psa musi zaczynać się od dużego S");
        }
    }

    private void sprawdzRase(String rasa) {
        if (rasa == null) {
            throw new IllegalStateException("Rasa nie może być nullem");
        }
    }

    @Override
    public String toString() {
        return "Pies{" +
            "imie='" + imie + '\'' +
            ", rasa='" + rasa + '\'' +
            '}';
    }
}
