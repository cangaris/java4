package generics;

public class Main {
    public static void main(String[] args) {

        Box<String> boxStr = new Box<>("damian");

        Box<Long> boxLong = new Box<>(1L);

        Box<Dog> boxDog = new Box(new Dog());

        Box<Mops> mopsBox = new Box(new Mops());
    }
}
