package generics;

public class Box<E> {

    private E value;

    public Box(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }
}
