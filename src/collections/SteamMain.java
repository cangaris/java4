package collections;

import java.util.LinkedList;

public class SteamMain {
    public static void main(String[] args) {

        var names = new LinkedList<String>();
        names.add("Damian");
        names.add("Piotr");
        names.add("Jan");
        names.add("Joanna"); // 5
        names.add("Basia"); // 4
        names.add("Kasia"); // 4

        var woman = names.stream()
            .filter(name -> {
                var lastLetter = name.length() - 1;
                return name.charAt(lastLetter) == 'a';
            })
            .map(name -> name.toUpperCase())
            .limit(2)
            .toList();

        System.out.println(woman);
        System.out.println("------");

        var names2 = new LinkedList<String>();
        names2.add("Damian");
        names2.add("Piotr");
        names2.add("Jan");
        names2.add("Joanna"); // 5
        names2.add("Basia"); // 4
        names2.add("Kasia"); // 4

        var nameStartsWithD = names2.stream()
            .allMatch(name -> name.charAt(0) == 'Z');

        System.out.println("Czy wszystkie elementy zaczynają się na Z?: " + nameStartsWithD);
    }
}
