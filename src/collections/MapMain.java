package collections;

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class MapMain {

    public static void main(String[] args) {

        var list = new ArrayList<>();
        list.add("Damian");
        list.add("Nowak");
        list.add("info@info.pl");
        list.add("7768768768");

        System.out.println("Twój email to: " + list.get(2));

        var map = new HashMap<>();
        map.put("firstName", "Damian");
        map.put("lastName", "Nowak");
        map.put("email", "info@info.pl");
        map.put("phone", "7768768768");

        System.out.println(map);
        System.out.println("Twój email to: " + map.get("email"));
    }
}
