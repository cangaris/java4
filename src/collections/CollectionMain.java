package collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class CollectionMain {

    public static void main(String[] args) {

        /**
         * NIE OK:
         * pobrać element po indeksie
         * edycja po konkretnym indeksie
         * OK:
         * jeżeli chcemy dodawać nowe elementy
         * usuwanie elementów
         */
        var linkedList = new LinkedList<>();
        linkedList.add("Damian");
        linkedList.add("Piotr");
        linkedList.add("Jan");
        linkedList.add("Asia");
        linkedList.add("Basia");
        linkedList.add("Kasia");

        /**
         * OK:
         * pobrać element po indeksie
         * edycja po konkretnym indeksie
         * NIE OK:
         * jeżeli chcemy dodawać nowe elementy
         * usuwanie elementów
         */
        var list = new ArrayList<>();
        list.add("Damian");
        list.add("Piotr");
        list.add("Jan");
        list.add("Asia");
        list.add("Basia");
        list.add("Kasia");

        list.remove("Damian");

        list.set(1, "Janek"); // arr[1] = 'Janek'

        System.out.println(list.size());


        System.out.println("----");
        System.out.println(list.get(1)); // arr[1]
        System.out.println("----");

        list.forEach(element -> System.out.print(element));

        for (var element : list) {
            System.out.println(element);
        }

        System.out.println("----");

        var set = new HashSet<>();
        set.add("Damian");
        set.add("Piotr");
        set.add("Jan");
        set.add("Damian");

        set.contains("Damian");

         set.forEach(element -> System.out.println(element));
    }
}
