package calc;

import java.util.Scanner;

class Calculate {

    static void sqrt(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        if (number1 <= 0) {
            System.out.println("Wymagana jest liczba większa równa zero.");
            return;
        }
        if (number2 == 0) {
            System.out.println("Niedozwolona operacja dzielenia przez zero.");
            return;
        }
        var result = Math.pow(number1, 1 / number2); // Math.sqrt (potęgowanie 2 stopnia)
        System.out.println("Wynik pierwiatkowania kwadratowego to: " + result);
    }

    static void pow(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        var result = Math.pow(number1, number2);
        System.out.println("Wynik potęgowania to: " + result);
    }

    static void plus(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        var result = number1 + number2;
        System.out.println("Wynik dodawania to: " + result);
    }

    static void minus(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        var result = number1 - number2;
        System.out.println("Wynik odejmowania to: " + result);
    }

    static void multiply(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        var result = number1 * number2;
        System.out.println("Wynik mnożenia to: " + result);
    }

    static void divide(Scanner inputScanner, String[] args) {
        var number1 = getParameter(inputScanner, args);
        if (number1 == null) {
            return;
        }
        var number2 = getParameter(inputScanner, args);
        if (number2 == null) {
            return;
        }
        if (number2 == 0) {
            System.out.println("Niedozwolona operacja dzielenia przez zero.");
            return;
        }
        var result = number1 / number2;
        System.out.println("Wynik dzielenia to: " + result);
    }

    static Float getParameter(Scanner inputScanner, String[] args) {
        System.out.print("Wpisz liczbę: ");
        try {
            return inputScanner.nextFloat();
        } catch (Exception exception) {
            System.out.println("Wymagane jest wprowadzenie wyłącznie liczby.");
            Calculator.main(args);
            return null;
        }
    }
}
