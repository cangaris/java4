package calc;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        var inputScanner = new Scanner(System.in);

        System.out.println("-----------");
        System.out.println("Wybierz typ operacji:");
        System.out.println("[A] dodawanie, [B] odejmowanie, [C] mnożenie, [D] dzielenie, [E] potęgowanie, [F] pierwiastkowanie");
        System.out.println("-----------");

        var operation = inputScanner.next().toUpperCase();
        switch (operation) {
            case "A" -> Calculate.plus(inputScanner, args);
            case "B" -> Calculate.minus(inputScanner, args);
            case "C" -> Calculate.multiply(inputScanner, args);
            case "D" -> Calculate.divide(inputScanner, args);
            case "E" -> Calculate.pow(inputScanner, args);
            case "F" -> Calculate.sqrt(inputScanner, args);
            default -> System.out.println("Nierozpoznany typ operacji.");
        }
    }
}
