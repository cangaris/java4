package enums;

public enum Colors {
    GREEN,
    RED,
    BLUE;

    static Colors getDefaultColor() {
        return RED;
    }
}
