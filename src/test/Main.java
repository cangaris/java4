package test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now().minusDays(2));
        dates.add(LocalDate.now().minusDays(4));
        dates.add(LocalDate.now().minusDays(1));
        dates.add(LocalDate.now().minusDays(5));
        dates.add(LocalDate.now().minusDays(8));

        fillFieldsWithPaymentsLabels(dates);
    }

    static final List<String> LABELS = Stream.of("first", "second", "third", "fourth")
        .map(s -> s + "InstallmentPaymentDeadline")
        .toList();

    private record Data(LocalDate date, String label) {
    }

    private static void fillFieldsWithPaymentsLabels(List<LocalDate> finalPaymentsDates) {
        finalPaymentsDates.sort(Comparator.naturalOrder());

        var size = Math.min(finalPaymentsDates.size(), LABELS.size());
        var list = IntStream.range(0, size)
            .mapToObj(value -> new Data(finalPaymentsDates.get(value), LABELS.get(value)))
            .collect(Collectors.toMap(Data::label, Data::date));

        System.out.println(list);
    }
}
