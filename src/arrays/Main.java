package arrays;

public class Main {

    public static void main(String[] args) {

        var wintersMonths = new String[] { "grudzień", "styczeń", "luty" };

        for ( var month : wintersMonths ) {
            System.out.println(month);
        }

        System.out.println("------");

        var daysOfWeek = new String[7];

        daysOfWeek[0] = "poniedziałek";
        daysOfWeek[1] = "wtorek";
        daysOfWeek[2] = "środa";
        daysOfWeek[3] = "czwartek";
        daysOfWeek[4] = "piątek";
        daysOfWeek[5] = "sobota";
        daysOfWeek[6] = "niedziela";

        System.out.println("Rozmiar tablicy: " + daysOfWeek.length);
        System.out.println("------");

        // i++ ( i = i + 1 )
        for ( int i = daysOfWeek.length - 1; i >= 0; i-- ) {
            System.out.println( daysOfWeek[i] );
        }
        System.out.println("------");

        for ( var day : daysOfWeek ) {
            System.out.println(day);
        }
        System.out.println("------");

        int i = daysOfWeek.length - 1;
        while (i >= 0) {
            System.out.println(daysOfWeek[i]);
            i--;
        }
        System.out.println("------");

        i = 0;
        while (i < 10) { // true
            i++; // 1
            if (i % 2 != 0) {
                continue; // przerwya tylko biezaca interace, pozwana na nastepne iteracje
                // break; // przerywa bezwglednie petle
            }
            System.out.println(i);
        }

        System.out.println("------");

        int temperature = 39;
        if (temperature < 36) {
            System.out.println("Jesteś osłabiony?");
        }
        else if (temperature < 37) {
            System.out.println("Wszystko w normie!");
        }
        else if (temperature < 38) {
            System.out.println("Jesteś przeziębiony?");
        }
        else {
            System.out.println("Masz co najmniej 38 stopni! Biegiem do lekarza!");
        }
    }
}
