package oop;

import java.util.Objects;

class Dog {

    private String name;
    private int age;
    private String breed;

    Dog(String name, int age, String breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    void bark () {
        System.out.println("pies " + name + ", szczeka: wow wow!");
    }

    void setName(String name) {
        this.name = name;
    }

    void setAge(int age) {
        if (age >= 0 && age <= 25 && age > this.age) {
            this.age = age;
        } else {
            throw new IllegalStateException("Niedozwolony wiek psa!");
        }
    }

    int getAge() {
        return age;
    }

    String getBreed() {
        return breed;
    }

    String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Dog{" +
            "name='" + name + '\'' +
            ", breed='" + breed + '\'' +
            ", age=" + age +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Dog dog = (Dog) o;

        if (age != dog.age) {
            return false;
        }
        if (!Objects.equals(name, dog.name)) {
            return false;
        }
        return Objects.equals(breed, dog.breed);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (breed != null ? breed.hashCode() : 0);
        return result;
    }
}
