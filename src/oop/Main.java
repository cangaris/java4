package oop;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Dog dog1 = new Dog("Szarik", 6, "mops"); // a1
        Dog dog2 = new Dog("Szarik", 6, "mops"); // e4
        Dog dog3 = new Dog("Szarik", 6, "mops"); // d7

        ArrayList<Dog> list = new ArrayList<>();
        list.add(dog1);
        list.add(dog2);
        list.add(dog3);

        Dog dog4 = dog1; // a1

        dog1.setName("Charlie");
        dog4.setAge(16);

        System.out.println(dog1.hashCode()); // a1 - ten sam hash ten sam obj (dog1 i dog4)
        System.out.println(dog2.hashCode()); // e4 - ten sam hash code ale inne miejsce w pamieci co dog3
        System.out.println(dog3.hashCode()); // d7
        System.out.println(dog4.hashCode()); // a1

        if (dog1.equals(dog3)) {
            System.out.println("dog1 jest rowny dog3");
        } else {
            System.out.println("dog1 NIE jest rowny dog3");
        }

        System.out.println(dog1);
        System.out.println(dog2);
        System.out.println(dog3);

        System.out.println("Wiek psa ( " + dog1.getName() + " ) to: " + dog1.getAge() + " lat");
        System.out.println("Wiek psa ( " + dog2.getName() + " ) to: " + dog2.getAge() + " lat");
        System.out.println("Wiek psa ( " + dog3.getName() + " ) to: " + dog3.getAge() + " lat");
    }
}
