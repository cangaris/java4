Link do YT:
[<a href="https://www.youtube.com/playlist?list=PLjjHeas5fLBQYTNWgtPtGzzyIcMicjKNJ">PLAYLISTA</a>]
<hr/>
Zadanie 1 - Do programu kalkulator dodać:
<ul>
<li>obsługę funkcji potęgowania (opcja E)</li>
<li>obsługę funkcji pierwiastkowania (opcja F)</li>
<li>należy googoglować jak w Javie wykonać wyżej wymienione operacje</li>
<li>zakładamy, że osługujemy tylko pierwiastek kwadratowy (2 stopnia)</li>
</ul>
<hr />
Lista tematów (22-23.10.2022):
<ul>
<li>https://www.samouczekprogramisty.pl/obiekty-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/porownywanie-obiektow-metody-equals-i-hashcode-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/metody-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/typy-proste-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/modyfikatory-dostepu-w-jezyku-java/</li>
</ul>
<hr />
Zadanie domowe (05.10.2022):
<ul>
<li>https://www.samouczekprogramisty.pl/zestaw-cwiczen-dla-poczatkujacych-programistow/#metody-w-j%C4%99zyku-java</li>
<li>https://www.samouczekprogramisty.pl/zestaw-cwiczen-dla-poczatkujacych-programistow/#obiekty-i-pakiety</li>
</ul>
<hr />
Lista tematów (05.10.2022):
<ul>
<li>https://www.samouczekprogramisty.pl/tablice-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/petle-i-instrukcje-warunkowe-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/dziedziczenie-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/wyjatki-w-jezyku-java/</li>
</ul>
<hr />
Lista tematów (06.10.2022):
<ul>
<li>https://www.samouczekprogramisty.pl/interfejsy-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/typ-wyliczeniowy-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/kolekcje-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/typy-generyczne-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/strumienie-w-jezyku-java/</li>
<li>https://www.samouczekprogramisty.pl/wyrazenia-lambda-w-jezyku-java/</li>
</ul>
<hr />
Zadanie 2 (17.12.2022):
<ul>
<li> 1. przefitrować listę aby zwróciła gry, które są tylko single player (1)</li>
<li> 2. zmienić filtrowanie z single player (1 gracz) na multiplayer (2+)</li>
<li> 3. przefiltrować gry z oceną większa rowna 8</li>
<li> 4. gry powinny być tańsze lub rowne niż 200 zł</li>
<li>https://stackoverflow.com/questions/34677644/how-to-use-comparison-operators-like-on-bigdecimal</li>
</ul>
<hr />
